import { userBuilder } from "../builder/User";

describe("RegisterPage", () => {
  it("User can register", () => {
    const user = userBuilder({})();
    cy.visit("/register");
    cy.findByLabelText(/name/i).type(user.name);
    cy.findByLabelText(/email/i).type(user.email);
    cy.findByLabelText(/password/i).type(user.password);
    cy.findByText(/register/i)
      .parent()
      .click();
    cy.url().should("contain", "/dashboard");
  });
});
