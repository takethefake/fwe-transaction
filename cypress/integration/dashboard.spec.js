import { userBuilder } from "../builder/User";
import { transactionBuilder } from "../builder/Transaction";

describe("DashboardPage", () => {
  it("can create a new transaction", () => {
    cy.loginNewUser({}).then(user => {
      const transaction = transactionBuilder({})();
      cy.visit("/dashboard");
      cy.findByText(/create transaction/i)
        .parent()
        .click();
      cy.findByLabelText(/name/i).type(transaction.name);
      cy.findByLabelText(/value/i).type(transaction.value);
      cy.findByLabelText(/description/i).type(transaction.description);
      cy.findByLabelText(/tags/i).type("TestTag {enter}", { force: true });
      cy.findByText(/ok/i)
        .parent()
        .click();
      cy.findByTestId("transaction-item").should("have.length", 1);
    });
  });
  it.only("can logout", () => {
    cy.loginNewUser({}).then(user => {
      cy.visit("/dashboard");
      cy.findByText(/logout/i)
        .parent()
        .click();
      cy.url().should("contain", "/login");
    });
  });
});
