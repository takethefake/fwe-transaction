import { userBuilder } from "../builder/User";

describe("LoginPage", () => {
  it("should render login Page", () => {
    cy.visit("/");
    cy.url().should("contain", "/login");
  });

  it("can move to RegisterPage", () => {
    cy.visit("/");
    cy.findByText(/register/i).click();
    cy.url().should("contain", "/register");
  });

  it("can login", () => {
    cy.createUser({}).then(user => {
      cy.visit("/login");
      cy.findByLabelText(/email/i).type(user.email);
      cy.findByLabelText(/password/i).type(user.password);
      cy.findByText(/login/i)
        .parent()
        .click();
      cy.url().should("contain", "/dashboard");
    });
  });
});
