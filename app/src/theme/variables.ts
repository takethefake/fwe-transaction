// Custom definitions
export const colors = {
  grey: {
    dark: "rgb(32,32,32)",
    mid: "rgb(45,45,45)",
    light: "rgb(61,61,61)"
  },
  primary: "rgb(54, 161, 139)",
  white: "#ffffff"
};

// Theme colors
export const primaryColor = colors.primary;
export const infoColor = colors.primary;
export const successColor = "#52c41a"; // success state color
export const warningColor = "#faad14"; // warning state color
export const errorColor = "#f5222d"; // error state color
export const fontSizeBase = "16px"; // major text font size
export const headingColor = "rgba(255, 255, 255, 0.85)"; // heading text color
export const textColor = "rgba(255, 255, 255, 0.65)"; // major text color
export const textColorSecondary = "rgba(255, 255, 255, .45)"; // secondary text color
export const disabledColor = "rgba(37, 111, 96, 1)"; // disable state color
export const borderRadiusBase = "4px"; // major border radius
export const borderColorSplit = colors.grey.mid; // major border color
export const borderColorBase = colors.grey.mid;
export const disabledBg = colors.grey.dark;

export const boxShadowBase = "0 2px 8px rgba(0, 0, 0, 0.15)"; // major shadow for layers
export const componentBackground = colors.grey.mid;

export const bodyBackground = colors.grey.dark;
export const layoutBodyBackground = colors.grey.dark;
export const layoutHeaderBackground = colors.primary;
export const layoutFooterBackground = layoutHeaderBackground;
export const layoutHeaderHeight = "64px";
export const layoutHeaderPadding = "0 38px";
export const layoutFooterPadding = "24px 50px";
export const layoutSiderBackground = layoutHeaderBackground;
export const layoutSiderBackgroundLight = colors.white;

export const inputBg = colors.grey.light;
