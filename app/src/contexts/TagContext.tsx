import React from "react";
import { Tag } from "../pages/Dashboard/components/TransactionItem";
import { useFetch } from "../hooks/useFetch";

type TagContext = {
  tags: Tag[];
  actions: {
    refetchTags: () => Promise<void>;
  };
};
export const initialContext = {
  tags: [],
  actions: {
    refetchTags: async () => {}
  }
};
export const tagContext = React.createContext<TagContext>(initialContext);

export const TagProvider: React.FC = ({ children }) => {
  const { data, refetch } = useFetch<Tag[]>("/api/tag");
  return (
    <tagContext.Provider
      value={{ tags: data || [], actions: { refetchTags: refetch } }}
    >
      {children}
    </tagContext.Provider>
  );
};
