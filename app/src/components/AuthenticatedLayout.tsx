import React, { useContext } from "react";
import { Layout, Button } from "antd";
import { authContext } from "../contexts/AuthenticationContext";

export const AuthenticatedLayout: React.FC = ({ children }) => {
  const {
    actions: { logout }
  } = useContext(authContext);
  return (
    <Layout>
      <Layout.Header
        style={{
          display: "flex",
          justifyContent: "center  ",
          alignItems: "center"
        }}
      >
        <div style={{ flex: 1, fontSize: "1.5rem" }}>FWE 26</div>
        <Button
          onClick={() => {
            logout();
          }}
        >
          logout
        </Button>
      </Layout.Header>
      <Layout.Content style={{ padding: "1rem 4rem" }}>
        {children}
      </Layout.Content>
    </Layout>
  );
};
