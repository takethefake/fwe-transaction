import { Layout, Card } from "antd";
import React from "react";
import { styled } from "../theme";

export const UnauthenticatedLayout: React.FC = ({ children }) => {
  const Container = styled.div`
    display: flex;
    width: 650px;
    margin: auto;
    height: 100%;
    justify-content: center;
    align-items: center;
  `;
  return (
    <Container>
      <Card
        bordered={false}
        style={{
          boxShadow: "7px 6px 30px -8px rgba(0,0,0,0.75)",
          width: "60%"
        }}
      >
        {children}
      </Card>
    </Container>
  );
};
