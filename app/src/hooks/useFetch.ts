import { useEffect, useRef } from "react";
import { useState } from "react";
import { authContext, Response } from "./../contexts/AuthenticationContext";
import { useContext } from "react";
export function useFetch<T>(url: string) {
  const { token } = useContext(authContext);
  const [data, setData] = useState<T | null>(null);
  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState(false);
  const refetch = async () => {
    try {
      setLoading(true);
      const res = await fetch(url, {
        method: "GET",
        headers: { Authorization: `${token}` }
      });
      if (res.status === 200) {
        const { data } = (await res.json()) as Response<T>;
        setData(data);
      } else {
        const { status } = (await res.json()) as Response<undefined>;
        setError(status);
      }
      setLoading(false);
    } catch (e) {
      console.error(e);
    }
  };
  const refetchRef = useRef(refetch);

  useEffect(() => {
    if (token !== null) {
      refetchRef.current();
    }
  }, [token]);

  return { data, loading, error, refetch: refetchRef.current };
}
