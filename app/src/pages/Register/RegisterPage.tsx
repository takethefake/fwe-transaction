import React, { useContext } from "react";
import { routes } from "../../utils/routes";
import { Link } from "react-router-dom";
import { authContext } from "../../contexts/AuthenticationContext";
import { Formik } from "formik";
import { SubmitButton, Form, Input } from "formik-antd";
import * as Yup from "yup";
import { UnauthenticatedLayout } from "../../components/UnauthenticatedLayout";
import { Alert } from "antd";

const RegisterSchema = Yup.object().shape({
  email: Yup.string()
    .email("Invalid email")
    .required("Required"),
  name: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  password: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required")
});
export const RegisterPage = () => {
  const {
    actions: { register }
  } = useContext(authContext);
  return (
    <UnauthenticatedLayout>
      <Formik
        validationSchema={RegisterSchema}
        initialValues={{
          name: "",
          email: "",
          password: "",
          general: ""
        }}
        onSubmit={async ({ name, email, password }, actions) => {
          try {
            await register({ email, password, name });
          } catch (e) {
            actions.setFieldError("general", e.message);
            actions.setSubmitting(false);
          }
        }}
      >
        {formik => (
          <Form onSubmit={formik.handleSubmit}>
            {formik.errors.general ? (
              <Alert message={formik.errors.general} type="error" />
            ) : null}
            <Form.Item name="name" label="Name" htmlFor="name">
              <Input id="name" size="large" name="name" />
            </Form.Item>
            <Form.Item name="email" label="Email" htmlFor="email">
              <Input id="email" size="large" name="email" />
            </Form.Item>
            <Form.Item name="password" label="Password" htmlFor="password">
              <Input.Password id="password" size="large" name="password" />
            </Form.Item>
            <SubmitButton size="large">Register</SubmitButton>
            <p>
              Back to <Link to={routes.app.login}>Login!</Link>
            </p>
          </Form>
        )}
      </Formik>
    </UnauthenticatedLayout>
  );
};
