import React from "react";
import { render, fireEvent } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import {
  authContext,
  AuthProvider,
  RegisterOptions,
  initialAuthContext
} from "../../contexts/AuthenticationContext";
import { RegisterPage } from "./RegisterPage";
import { act } from "react-dom/test-utils";

describe("RegisterPage", () => {
  it("renders properly", () => {
    const { getByLabelText, getByText } = render(
      <BrowserRouter>
        <authContext.Provider value={{ ...initialAuthContext }}>
          <RegisterPage />
        </authContext.Provider>
      </BrowserRouter>
    );
    const email = getByLabelText(/email/i);
    const password = getByLabelText(/password/i);
    const name = getByLabelText(/name/i);
    const registerButton = getByText(/register/i);
  });

  it("can register", async () => {
    const registerMock = jest.fn();
    const { getByLabelText, getByText } = render(
      <BrowserRouter>
        <authContext.Provider
          value={{
            ...initialAuthContext,
            actions: {
              ...initialAuthContext.actions,
              register: registerMock
            }
          }}
        >
          <RegisterPage />
        </authContext.Provider>
      </BrowserRouter>
    );
    const email = getByLabelText(/email/i);
    const password = getByLabelText(/password/i);
    const name = getByLabelText(/name/i);
    const registerButton = getByText(/register/i).closest("button");
    fireEvent.change(email, { target: { value: "test@test.de" } });
    fireEvent.change(password, { target: { value: "password" } });
    fireEvent.change(name, { target: { value: "Tester" } });
    await act(async () => {
      fireEvent.click(registerButton!);
    });
    const {
      email: emailValue,
      password: passwordValue,
      name: nameValue
    } = registerMock.mock.calls[0][0] as RegisterOptions;
    expect(registerMock.mock.calls.length).toBe(1);
    expect(emailValue).toBe("test@test.de");
    expect(passwordValue).toBe("password");
    expect(nameValue).toBe("Tester");
  });
});
