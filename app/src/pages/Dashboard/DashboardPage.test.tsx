import React from "react";
import { render, fireEvent, waitForElement } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import { AuthProvider } from "../../contexts/AuthenticationContext";
import { DashboardPage } from "./DashboardPage";
import { FetchMock } from "jest-fetch-mock/types";
import { Transaction, TransactionType } from "./components/TransactionItem";
import { Response } from "../../contexts/AuthenticationContext";
import { act } from "react-dom/test-utils";
import { TagProvider } from "../../contexts/TagContext";

describe("DashboardPage", () => {
  beforeEach(() => {
    (fetch as FetchMock).resetMocks();
  });
  it("adds an Item after Create", async () => {
    const transactionInitialFetchResponse: Response<Transaction[]> = {
      status: "ok",
      data: []
    };

    const transactionResponse: Response<Transaction> = {
      status: "ok",
      data: {
        id: "1",
        createdAt: new Date("2019-01-01"),
        updatedAt: new Date("2019-01-01"),
        description: "Testdescription",
        name: "TestName",
        tags: [],
        type: TransactionType.EXPENSE,
        value: 2.5
      }
    };
    (fetch as FetchMock)
      .once(JSON.stringify(transactionResponse))
      .once(JSON.stringify(transactionInitialFetchResponse));
    const { getByLabelText, debug, getByText, getByTestId } = render(
      <BrowserRouter>
        <AuthProvider>
          <DashboardPage />
        </AuthProvider>
      </BrowserRouter>
    );
    const createTransaction = getByText(/create transaction/i).closest(
      "button"
    );

    await act(async () => {
      fireEvent.click(createTransaction!);
    });

    const name = getByLabelText(/name/i);
    const value = getByLabelText(/value/i);
    const description = getByLabelText(/description/i);
    fireEvent.change(name, {
      target: { value: transactionResponse.data.name }
    });
    fireEvent.change(value, {
      target: { value: transactionResponse.data.value }
    });
    fireEvent.change(description, {
      target: { value: transactionResponse.data.description }
    });

    const submit = getByText(/ok/i);
    fireEvent.click(submit);
    await waitForElement(() => getByTestId("transaction-item"));
    expect(getByTestId("transaction-item").children.length).toBe(1);
  });
});
