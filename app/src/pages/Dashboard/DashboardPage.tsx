import React, { useState, useContext, useEffect } from "react";
import {
  TransactionItem,
  TransactionList,
  Transaction
} from "./components/TransactionItem";
import {
  CreateTransaction,
  CreateTransactionData
} from "./components/CreateTransaction";
import { authContext, Response } from "../../contexts/AuthenticationContext";
import { useFetch } from "../../hooks/useFetch";
import { Button, Icon, Card } from "antd";
import { AuthenticatedLayout } from "../../components/AuthenticatedLayout";

export const DashboardPage: React.FC = () => {
  const { token } = useContext(authContext);
  const [createModalIsVisible, setCreateModalIsVisible] = useState(false);
  const [transactions, setTransactions] = useState<Transaction[]>([]);
  const { data, loading } = useFetch<Transaction[]>("/api/transaction");
  useEffect(() => {
    if (data !== null && !loading) {
      setTransactions(data);
    }
  }, [data, loading]);

  const onCreateTransaction = async (
    transactionData: CreateTransactionData
  ) => {
    const res = await fetch("/api/transaction", {
      method: "POST",
      headers: {
        Authorization: `${token}`,
        "Content-Type": "application/json"
      },
      body: JSON.stringify(transactionData)
    });
    const { data: newTransaction } = (await res.json()) as Response<
      Transaction
    >;

    setTransactions([...transactions, newTransaction]);
  };

  return (
    <AuthenticatedLayout>
      <div
        style={{
          display: "flex",
          padding: "1.5rem 0",
          justifyContent: "flex-end"
        }}
      >
        <Button onClick={() => setCreateModalIsVisible(true)}>
          <Icon type="plus" /> Create Transaction
        </Button>
      </div>
      <CreateTransaction
        onCreateTransaction={onCreateTransaction}
        modalIsVisible={createModalIsVisible}
        setModalIsVisible={setCreateModalIsVisible}
      />

      <TransactionList>
        {loading ? (
          <Card bordered={false} loading={true} />
        ) : (
          transactions.map(transaction => {
            return (
              <TransactionItem key={transaction.id} transaction={transaction} />
            );
          })
        )}
      </TransactionList>
    </AuthenticatedLayout>
  );
};
