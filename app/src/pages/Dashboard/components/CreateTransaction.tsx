import React, { useContext } from "react";
import { TransactionType, Tag } from "./TransactionItem";
import { MultiCreateable } from "./MultiCreateable";
import { tagContext } from "../../../contexts/TagContext";
import { Formik, Field, FieldProps } from "formik";
import * as Yup from "yup";
import { Form, Input, InputNumber, Checkbox } from "formik-antd";
import { Modal } from "antd";

export type CreateTransactionData = {
  name: string;
  description: string;
  value: number;
  type: TransactionType;
  tags: Tag[];
};

export type CreateTransactionProps = {
  onCreateTransaction: (data: CreateTransactionData) => void;
  modalIsVisible: boolean;
  setModalIsVisible: any;
};

const CreateTransactionSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  description: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  value: Yup.number()
    .positive("has to be positive")
    .required("Required")
});

export const CreateTransaction: React.FC<CreateTransactionProps> = ({
  onCreateTransaction,
  modalIsVisible,
  setModalIsVisible
}) => {
  const {
    tags: options,
    actions: { refetchTags }
  } = useContext(tagContext);

  return (
    <Formik
      initialValues={{
        name: "",
        description: "",
        value: "",
        isExpense: true,
        tags: []
      }}
      validationSchema={CreateTransactionSchema}
      onSubmit={async ({ name, description, value, isExpense, tags }) => {
        await onCreateTransaction({
          name,
          description,
          value: parseFloat(value) * 100,
          type: isExpense ? TransactionType.EXPENSE : TransactionType.INCOME,
          tags
        });
        refetchTags();
      }}
    >
      {formik => (
        <Modal
          centered
          visible={modalIsVisible}
          onOk={() => {
            formik.handleSubmit();
            setModalIsVisible(false);
          }}
          onCancel={() => setModalIsVisible(false)}
        >
          <Form onSubmit={formik.handleSubmit}>
            <Form.Item label="Name" name="name" htmlFor="name">
              <Input id="name" name="name" size="large" />
            </Form.Item>
            <Form.Item label="Value" name="value" htmlFor="value-number-input">
              <Input id="value-number-input" name="value" size="large" />
            </Form.Item>
            <Form.Item
              label="Description"
              name="description"
              htmlFor="description"
            >
              <Input.TextArea id="description" name="description" />
            </Form.Item>

            <Form.Item
              label="Is it an Expense?"
              name="isExpense"
              htmlFor="isExpense"
            >
              <Checkbox id="isExpense" name="isExpense" />
            </Form.Item>
            <Form.Item label="Tags" name="tags" htmlFor="react-select-2-input">
              <Field name="tags">
                {({ field }: FieldProps) => (
                  <MultiCreateable
                    values={field.value}
                    options={options}
                    setValue={value => {
                      formik.setFieldValue("tags", value);
                    }}
                  ></MultiCreateable>
                )}
              </Field>
              />
            </Form.Item>
          </Form>
        </Modal>
      )}
    </Formik>
  );
};
