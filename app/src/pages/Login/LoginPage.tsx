import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { routes } from "../../utils/routes";
import { authContext } from "../../contexts/AuthenticationContext";
import { Formik } from "formik";
import * as Yup from "yup";
import { UnauthenticatedLayout } from "../../components/UnauthenticatedLayout";
import { Form, Input, SubmitButton } from "formik-antd";
import { Alert } from "antd";
import { act } from "react-dom/test-utils";

const LoginSchema = Yup.object().shape({
  email: Yup.string()
    .email("Invalid email")
    .required("Required"),
  password: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required")
});

export const LoginPage: React.FC = () => {
  const {
    actions: { login }
  } = useContext(authContext);
  return (
    <UnauthenticatedLayout>
      <Formik
        initialValues={{ email: "", password: "", general: "" }}
        onSubmit={async ({ email, password }, actions) => {
          try {
            await login({ email, password });
          } catch (e) {
            console.log(e);
            actions.setFieldError("general", e.message);
            actions.setSubmitting(false);
          }
        }}
        validationSchema={LoginSchema}
      >
        {formik => (
          <Form onSubmit={formik.handleSubmit}>
            {formik.errors.general ? (
              <Alert message={formik.errors.general} type="error" />
            ) : null}
            <Form.Item label="Email" name="email" htmlFor="email">
              <Input id="email" name="email" size="large" />
            </Form.Item>
            <Form.Item label="Password" name="password" htmlFor="password">
              <Input.Password
                id="password"
                name="password"
                size="large"
                aria-label="Password"
              />
            </Form.Item>
            <SubmitButton size="large">login</SubmitButton>
            <p>
              Have no account? <Link to={routes.app.register}>Register!</Link>
            </p>
          </Form>
        )}
      </Formik>
    </UnauthenticatedLayout>
  );
};
