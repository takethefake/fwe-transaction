import React from "react";
import { render, fireEvent } from "@testing-library/react";
import { LoginPage } from "./LoginPage";
import { BrowserRouter } from "react-router-dom";
import {
  authContext,
  initialAuthContext
} from "../../contexts/AuthenticationContext";
import { act } from "react-dom/test-utils";
describe("LoginPage", () => {
  it("renders properly", () => {
    const loginMock = jest.fn();
    const { getByLabelText, getByText, debug } = render(
      <BrowserRouter>
        <authContext.Provider
          value={{
            ...initialAuthContext,
            actions: { ...initialAuthContext.actions, login: loginMock }
          }}
        >
          <LoginPage />
        </authContext.Provider>
      </BrowserRouter>
    );
    const email = getByLabelText(/email/i);
    const password = getByLabelText(/password/i);
    const login = getByText(/login/i);
  });
  it("can login", async () => {
    const loginMock = jest.fn();
    const { getByLabelText, getByText, debug } = render(
      <BrowserRouter>
        <authContext.Provider
          value={{
            ...initialAuthContext,
            actions: { ...initialAuthContext.actions, login: loginMock }
          }}
        >
          <LoginPage />
        </authContext.Provider>
      </BrowserRouter>
    );
    const email = getByLabelText(/email/i);
    const password = getByLabelText(/password/i);
    const login = getByText(/login/i).closest("button");
    fireEvent.change(email, { target: { value: "test@test.de" } });
    fireEvent.change(password, { target: { value: "password" } });
    await act(async () => {
      fireEvent.click(login!);
    });
    expect(loginMock.mock.calls.length).toBe(1);
    expect(loginMock.mock.calls[0][0].email).toBe("test@test.de");
    expect(loginMock.mock.calls[0][0].password).toBe("password");
  });
});
