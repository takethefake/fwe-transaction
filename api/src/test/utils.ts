import { Repository, getRepository } from "typeorm";
import { User } from "../entity/User";
import { Authentication } from "../module/authentication";
export async function createTestUser() {
  const userRepository: Repository<User> = getRepository(User);
  const hashedPassword: string = await Authentication.hashPassword(
    "einpasswort"
  );
  const testUser = new User();
  testUser.email = "eineemail";
  testUser.name = "einname";
  testUser.password = hashedPassword;
  const createdUser: User = await userRepository.save(testUser);
  return createdUser;
}
export async function authenticateTestUser(user: User) {
  const token = await Authentication.generateToken({
    id: user.id,
    email: user.email,
    name: user.name
  });
  return token;
}
